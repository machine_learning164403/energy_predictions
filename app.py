from flask import Flask, jsonify, request
from werkzeug.utils import secure_filename
import numpy as np
import uuid
import os

from scripts.utils import load_dataframe
from predict import predict_from_XGBRegressor_model

app = Flask(__name__)

UPLOAD_FOLDER = 'Data'
ALLOWED_EXTENSIONS = {'csv'}

np.set_printoptions(threshold=np.inf)

def allowed_file(filename):
    return '.' in filename and \
           filename.rsplit('.', 1)[1].lower() in ALLOWED_EXTENSIONS

def generate_unique_filename(original_filename):
    extension = '.' + original_filename.rsplit('.', 1)[1] if '.' in original_filename else ''
    unique_id = uuid.uuid4()  # Generate a universally unique identifier
    safe_filename = secure_filename(str(unique_id) + extension)
    return safe_filename

@app.route('/', methods=['GET'])
def init():
    return 'App is running'

@app.route('/predict', methods=['POST'])
def predict():
    if 'file' not in request.files:
        return jsonify({"error": "No file part"}), 400
    
    file = request.files['file']

    if file.filename == '':
        return jsonify({"error": "No selected file"}), 400
    
    if file and allowed_file(file.filename):
        filename = generate_unique_filename(file.filename)
        file.save(os.path.join(UPLOAD_FOLDER, filename))

        # Load validation dataset:
        # X_val should come from the user input    
        X_val = load_dataframe(os.path.join(UPLOAD_FOLDER, filename))

        y_pred = predict_from_XGBRegressor_model(X_val)
        return str(y_pred)

    
if __name__ == '__main__':
    app.run(host='0.0.0.0', port=6000, debug=True)