import numpy as np
import pandas as pd
from xgboost import XGBRegressor
from scripts.utils import load_dataframe, calculate_mean_absolute_error
import os


data_dir = 'data/processed'
model_dir = 'models'
model_name = 'model_XGBoost_iteration_1.json'
path_to_saved_model = os.path.join(model_dir, model_name)

if not os.path.exists(model_dir):
    os.makedirs(model_dir)


# Load validation dataset:
# X_val should come from the user input    
X_val = load_dataframe(os.path.join(data_dir, "X_val.csv"))
y_val = load_dataframe(os.path.join(data_dir, "y_val.csv"))

def predict_from_XGBRegressor_model(X_val, path_to_saved_model):
    model = XGBRegressor()
    model.load_model(path_to_saved_model)    
    y_pred = model.predict(X_val)
    return y_pred


# These artifacts should be stored in the log directory
y_pred = predict_from_XGBRegressor_model(X_val, path_to_saved_model)
print(y_pred)

mean_absolute_error = calculate_mean_absolute_error(y_pred, y_val)
print(mean_absolute_error)