import pandas as pd
import numpy as np


def refactor_datetime(df, column):
    df['Hour'] = pd.to_datetime(df[column]).dt.hour
    df['Day'] = pd.to_datetime(df[column]).dt.day
    df['Month'] = pd.to_datetime(df[column]).dt.month
    df['Year'] = pd.to_datetime(df[column]).dt.year
    return df


def calculate_mean_absolute_error(y_val, y_pred):
    return np.mean(np.abs(y_val - y_pred))


def split_dataframe(df, column):
    return (df.drop(column, axis = 1), df[column])


def save_df_as_csv(df, out_filename):
    df.to_csv(out_filename, sep='\t', index=False)


def load_dataframe(dir):
    array = pd.read_csv(dir, index_col=False, sep='\t').to_numpy()
    return array


# Because we have potential outliers in the target variable, we can define
# a custom loss function for XGBoost, which would be Huber loss
def huber_loss(y_true, y_pred):
    d = y_pred - y_true
    scale = 1 + (d / 2)
    squared_loss = np.square(d) / 2
    linear_loss = np.abs(d) - 0.5
    return np.where(np.abs(d) < 1, squared_loss, linear_loss) * scale