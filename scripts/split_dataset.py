import numpy as np
import pandas as pd
from scripts.utils import split_dataframe, save_df_as_csv


# Split the data into train, validation, and test sets using X/Y/Z split,
# where for example X = 0.6, Y = 0.2, and Z = 0.2. Given the size of the dataset,
# that seems reasonable. Ideally these should be supplied by the user, i.e. passed through 
# 'argparse'
train_fraction = 0.6
val_fraction = test_fraction = 0.2

energy_df = pd.read_csv('../Data/processed/energy_v3.csv', sep='\t')

# I don't think the random sampling is appropriate:
# energy_df = energy_df.sample(frac=1)


train, val, test = np.split(energy_df, [int(train_fraction*len(energy_df)), int((train_fraction+val_fraction)*len(energy_df))])

X_train, y_train = split_dataframe(train, 'MWh')
X_val, y_val = split_dataframe(val, 'MWh')
X_test, y_test = split_dataframe(test, 'MWh')


X_train = save_df_as_csv(X_train, 'Data/processed/X_train.csv')
y_train = save_df_as_csv(y_train, 'Data/processed/y_train.csv')
X_val = save_df_as_csv(X_val, 'Data/processed/X_val.csv')
y_val = save_df_as_csv(y_val, 'Data/processed/y_val.csv')
X_test = save_df_as_csv(X_test, 'Data/processed/X_test.csv')
y_test = save_df_as_csv(y_test, 'Data/processed/y_test.csv')

