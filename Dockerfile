# Use an official Python runtime as a parent image
FROM python:3.9-slim

# Set the working directory in the container
WORKDIR /usr/src/app

# Copy the current directory contents into the container at the working directory
COPY . .

RUN apt-get update -y

RUN apt-get install gcc python3-dev -y

# Install any needed packages specified in requirements.txt
RUN pip install --no-cache-dir -r requirements.txt

# Port 5000 is available on Windows machine but on an Apple machine -- need to change it to 6000
# Make port 5000 available to the world outside this container
EXPOSE 6000

# Run app.py when the container launches
CMD ["python", "app.py"]