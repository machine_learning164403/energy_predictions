# Energy Predictions ML Challenge

## Goal behind the project

The outcome of this project was to simulate a simple asychronour request where a user can access the already trained model and submit the energy features to receive predictions (i.e. active losses). 

**The target audience** (i.e. persona) is a business stakeholder who would like to receive active losses based on several energy data, such as temperature, net transfer, etc so that they can use those predictions for forecasting and minimizing costs.

My assumption was that this persona was not interested in knowing the technical details of the model or the loss metrics, and therefore predictions would be sufficient. However for quality monitoring purposes, the model artificats are absolutely necessary, as mentioned below.

## Cloning the repository and installation of the Docker container

To get started, run these commands:
```
cd existing_repo
git remote add origin https://gitlab.com/machine_learning164403/energy_predictions.git
git branch -M main
git push -uf origin main
```

Please make sure that Docker is installed and active on your machine. Then run the following commands:
```
docker build -t flask-app .
docker run -p 6000:6000 flask-app
```

You can access the app using a client such as Postman, which allows you to submit a `.csv` file containing the data
you want to predict. The response (for now) will be a prediction file. In an ideal case, we would want to output much more information, such as error metrics, scatter plots, etc.





## TODO:
- [ ] Set up integration and unit tests for the Flask application
- [ ] Store useful model artificats, such as hyperparameters, error metrics, inference time, etc.
- [ ] Compare these metrics across multiple models, such as **LightGBM (another Gradient Boosting Model),  neural networks (such as LSTM or GRU), ensemble methods, or even Transformers**
- [ ] Resolve a few minor issues with data processing, including adding a time lag, encoding variables, feature scaling, etc.
- [ ] Since the response variable is rightly skewed, it may be worthwhile to experiment with custom loss functions
- [ ] **Most importantly:** To deploy this model in the production, a much robust architecture than the one used in the current Flask demo is needed. 










