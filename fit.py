import numpy as np
import pandas as pd
from xgboost import XGBRegressor
from sklearn.model_selection import GridSearchCV
from scripts.utils import load_dataframe, huber_loss, save_df_as_csv
import os


# Define parameters here but ideally they should be passed as arguments when calling the script
data_dir = 'data/processed'
model_dir = 'models'
model_name = 'model_XGBoost_iteration_1.json'
path_to_saved_model = os.path.join(model_dir, model_name)
results_dir = 'results'

if not os.path.exists(model_dir):
    os.makedirs(model_dir)


# Load the X- and y- training data
X_train = load_dataframe(os.path.join(data_dir, "X_train.csv"))
y_train = load_dataframe(os.path.join(data_dir, "y_train.csv"))

def fit_XGBRegressor_model(X_train, y_train, path_to_saved_model):
    model = XGBRegressor(n_estimators=300, max_depth=3, eta=0.1, subsample=0.5, colsample_bytree=0.8, obj=huber_loss)
    model.fit(X_train, y_train)
    model.save_model(path_to_saved_model)
    print(f'Model was saved successfully at: {path_to_saved_model}')

fit_XGBRegressor_model(X_train, y_train, path_to_saved_model)



# XGBoost model can be used for estimating feature importance:
xgb_model = XGBRegressor()
xgb_model.fit(X_train, y_train)

# Get feature importances
feature_importances = xgb_model.feature_importances_

# Display feature importances
for i, importance in enumerate(feature_importances):
    print(f"Feature {i}: {importance}")

top_n = 10
# These should be checked with the cross-correlation plot from the visualization part
most_important_indices = np.argsort(feature_importances)[-top_n:]
X_train_selected = X_train[:, most_important_indices]
X_train_selected_df = pd.DataFrame(X_train_selected, columns=most_important_indices)
X_train_selected = save_df_as_csv(X_train_selected_df, 'Data/processed/X_train_selected.csv')
 


# Of course, the many hyperparameters should be tuned to improve the model's performance.
# That could be done using validation set or doing a hyperparameter search

# Define the hyperparameter grid
param_grid = {
    'n_estimators': [100, 200, 300],
    'max_depth': [3, 4, 5],
    'learning_rate': [0.01, 0.1, 0.2],
    'subsample': [0.5, 0.8, 1],
    'colsample_bytree': [0.5, 0.8, 1]
}

# Create an instance of XGBRegressor
xgb_model = XGBRegressor()

# Create the grid search object
grid_search = GridSearchCV(estimator=xgb_model, param_grid=param_grid, cv=5, scoring='neg_mean_squared_error')

# Fit the grid search object to the data
grid_search.fit(X_train, y_train)

# Get the best combination of hyperparameters
best_params = grid_search.best_params_
print("Best hyperparameters:", best_params)
# Best hyperparameters: {'colsample_bytree': 0.8, 'learning_rate': 0.1, 'max_depth': 3, 'n_estimators': 300, 'subsample': 0.5}

# Train the model with the best hyperparameters
best_xgb_model = XGBRegressor(**best_params)
best_xgb_model.fit(X_train, y_train)

 